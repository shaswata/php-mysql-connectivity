/**
* Reset all errors in the DOM
*/
function resetErrors() {
    var error_elements = document.getElementsByClassName('input-error-msg');
    for(var loop_index = 0; loop_index < error_elements.length; loop_index++) {
        if(error_elements[loop_index] !== undefined) {
            error_elements[loop_index].innerHTML = "";
            error_elements[loop_index].style.display = "none";
        }
    }
}

// Reset all errors at page load
resetErrors();


/**
* Validate function
*/
var validateForm = function() {
    var error_exists = false;
    var form_name = "registration_form";

    /**
    * Display error id and the message
    */
    var displayError = function(error_element_id, message) {
        error_element = document.getElementById(error_element_id);
        error_element.innerHTML = message;
        error_element.style.display = "block";
        error_exists = true;
    }

    var validateUsername = function() {
        var id = "error_username";
        var x = document.forms[form_name]["username"].value.trim();
        if(x.length === 0) {
            displayError(id, "Please enter a username.");
        } else if(x.length > 20 ) {
            displayError(id, "Username should be less than 20 characters.");
        } else if(!x.match(/^[a-zA-Z0-9]+$/)) {
            displayError(id, "Username contains invalid characters. Only alphabets and numbers allowed.");
        }
    }

    var validateEmail = function() {
        var id = "error_email";

        // source : https://www.w3schools.com/jsref/prop_email_pattern.asp
        var regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;

        var x = document.forms[form_name]["email"].value.trim();
        if(x.length === 0) {
            displayError(id, "Please enter your E-Mail Address.");
        } else if(x.length > 255 ) {
            displayError(id, "Email should be less than 255 characters.");
        } else if(!x.match(regex)) {
            displayError(id, "Please enter a valid E-Mail Address.");
        }
    }

    var validatePassword = function() {
        var id = "error_password";

        var x = document.forms[form_name]["password"].value.trim();
        if(x.length === 0) {
            displayError(id, "Please enter a password.");
            x.value = "";
            return false;
        } else if(x.length < 8 || x.length > 16 ) {
            displayError(id, "Password should be 8 - 16 characters long");
            x.value = "";
            return false;
        }
        return x;
    }

    var validateRePassword = function() {
        var id = "error_re_password";

        var x = document.forms[form_name]["re-password"].value.trim();
        var y = validatePassword();
        if(y === false) {
            x.value = "";
            return;
        }
        if(x.length === 0) {
            displayError(id, "Please re-enter password.");
            x.value = "";
        } else if(x !== y) {
            displayError(id, "The passwords do not match.");
            x.value = "";
        }
    }

    resetErrors();

    validateUsername();
    validateEmail();
    validateRePassword();

    if(error_exists) {
        alert("There are errors in the form.");
        return !error_exists;
    }

    return true;
}
