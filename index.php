<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Form Validation Example</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style media="screen">
    body {
        background-color: #1b95e0;
    }
    .red {
        color: red;
    }
    .input-error-msg {
        display: none;
        padding: 5px 10px;
        margin-top: 5px;
    }
    </style>
</head>
<body>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Enter details to create account.</h4>
            </div>
            <div class="modal-body">
                <form id="registration_form" action="register.php" onsubmit="return validateForm()" method="POST" novalidate="novalidate">
                    <div class="form-group">
                        <label><span class="red">*</span>Username</label>
                        <input type="text" name="username" class="form-control" maxlength="20" placeholder="Choose an username">
                        <p class="text-left alert alert-danger input-error-msg" id="error_username"></p>
                    </div>
                    <div class="form-group">
                        <label><span class="red">*</span>Email address</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter email">
                        <p class="text-left alert alert-danger input-error-msg" id="error_email"></p>
                    </div>
                    <div class="form-group">
                        <label><span class="red">*</span>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter Password">
                        <p class="text-left alert alert-danger input-error-msg" id="error_password"></p>
                    </div>
                    <div class="form-group">
                        <label><span class="red">*</span>Repeat Password</label>
                        <input type="password" name="re-password" class="form-control" placeholder="Re-Enter Password">
                        <p class="text-left alert alert-danger input-error-msg" id="error_re_password"></p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <span class="red">*</span> Fields are necessary.
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" name="register" form="registration_form">Register</button>
                <hr>
                <div class="text-left">
                    <h4>List of Users</h4>
                    <hr>
                    <ul class="list-group">
                        <?php
                        require_once("inc/db.php");

                        $results = $con->query("SELECT * FROM users");

                        if($results->num_rows > 0) {
                            while($row = $results->fetch_assoc()) {
                                $username = $row['USERNAME'];
                                echo "<li class='list-group-item'>$username</li>";
                            }
                        } else {
                            echo "<li class='list-group-item'>No Users in the DB</li>";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->

    <!-- Include scripts -->
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>
